'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('api/auth/user', 'UserController.showUser')

Route.on('/').render('welcome')

Route.get('/videos', 'UserController.videos')

Route.group(()=>{
  Route.post('login', 'UserController.login')
  Route.post('register', 'UserController.register')
  //.validator('RegisterUser')
  Route.get('getuser/:id', 'UserController.show')
  Route.get('index', 'UserController.index')
}).prefix('users')
