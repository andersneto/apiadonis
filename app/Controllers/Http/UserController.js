'use strict'

const User = use('App/Models/User')

class UserController {
    async login({ request, response, auth}){
        const {email, password} = request.only(['email', 'password'])


        const user = await User.find({email: email})
        console.log(user)

      const responseAdapter = Object.assign({}, { user: await user.toJSON() })

      const token = await auth.withRefreshToken().attempt(email, password, responseAdapter)
      console.log(token)
      return response.json(token)
    }

    async register({request, response}){
        const {first_name, last_name, email, password} = request.only([
            'first_name',
            'last_name',
            'email',
            'password'
        ])

        await User.create({
            first_name,
            last_name,
            email,
            password
        })

        return response.send({message: 'Utilizador criado'})

    }

    async show({params, response}){
        const user = await User.find(params.id)
        const res = {
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
        }
        return response.json(res)
    }

    async index({params, response}){
        let entidade = await User.all();

        return response.json(entidade);
    }

    async videos({response}){
      const videos = [{
            id:'16',
            name: 'Intro to NuxtJS'
          },
          {
            id:'1',
            name: 'Intro to VueJS'
          },
          {
            id:'2',
            name: 'Intro to Teste'
          },
          {
            id:'71',
            name: 'Advanced Techniques for Library x'
          }]

      return response.json(videos);
    }

  async showUser({auth, response}){

    const currentUser = auth.getUser()
    const responseAdapter = Object.assign({}, { user: currentUser })

    //const user = auth.getUser()
    return response.json(responseAdapter)
  }

}

module.exports = UserController
